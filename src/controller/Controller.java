package controller;
import model.ModelManager;

import java.util.Scanner;


public class Controller {
    private int x;
    private int y;


    public void input(String s) {
        if (s.equals("w")) {
            ModelManager.movePlayer(0, -1);
        } else if (s.equals("a")) {
            ModelManager.movePlayer(-1, 0);
        } else if (s.equals("s")) {
            ModelManager.movePlayer(0, 1);
        } else if (s.equals("d")) {
            ModelManager.movePlayer(1, 0);
        }
    }


}
