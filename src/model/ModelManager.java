package model;

/**
 * Factory-Facade to allows the controller seamless interfacing with the model.
 * @author jdierberger1
 */
public class ModelManager {

	private static Player player = new Player(0, 0);

	public static int getPlayerX() {
		return player.getX();
	}

	public static int getPlayerY() {
		return player.getY();
	}

	public static int getPlayerHealth() {
		return player.getHealth();
	}

	public static void movePlayer(int dx, int dy) {
		player.setX(player.getX() + dx);
		player.setY(player.getY() + dy);
	}

	public static void healPlayer(int amount) {
		player.setHealth(player.getHealth() + amount);
	}

	public static void damagePlayer(int amount) {
		int newHealth = 0;
		player.setHealth(newHealth = player.getHealth() - amount < 0 ? 0 : newHealth);
	}

	public static void levelUpStat(String statName) {
		switch (statName.toLowerCase()) {
		case "marksmanship":
			player.setMarksmanship(player.getMarksmanship() + 1);
			break;
		case "persuasion":
			player.setPersuasion(player.getPersuasion() + 1);
			break;
		case "hackery":
			player.setHackery(player.getHackery() + 1);
			break;
		case "stealth":
			player.setStealth(player.getStealth() + 1);
			break;
		case "maxhealth":
			player.setMaxHealth(player.getMaxHealth() + 1);
			break;
		default:
			break;
		}
	}

	public static int getStat(String statName) {
		switch (statName.toLowerCase()) {
		case "marksmanship":
			return player.getMarksmanship();
		case "persuasion":
			return player.getPersuasion();
		case "hackery":
			return player.getHackery();
		case "stealth":
			return player.getStealth();
		case "maxhealth":
			return player.getMaxHealth();
		default:
			return -1;
		}
	}

	public static Item getItem(int slot) {
		return player.getFromInventory(slot);
	}

	public static Item removeItem(int slot) {
		Item item = player.getFromInventory(slot);
		player.editInventory(null, slot);
		return item;
	}

	public static boolean addItem(Item item) {
		int firstEmptyIndex = -1;
		for (int i = 0; i < player.getInventoryLength(); i++) {
			if (player.getFromInventory(i) == null) {
				firstEmptyIndex = i;
			}
			if (player.getFromInventory(i).getType().equals(item.getType())) {
				player.getFromInventory(i).setQuantity(player
					.getFromInventory(i).getQuantity() + item.getQuantity());
				return true;
			}
		}
		if (firstEmptyIndex != -1) {
			player.editInventory(item, firstEmptyIndex);
			return true;
		}
		return false;
	}

}
