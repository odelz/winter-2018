package model;
/**
 * Entity constructor
 * @author odelz
 */

class Entity {

    private String type;
    //location tracker
    private int x, y;

    // width and height of the hitbox
    private int hbWidth, hbHeight;

    //tracks entity's health if it has health
    private int health;

    //constructor
    Entity(String type, int x, int y, int w, int h) {
        this.type = type;
        this.x = x;
        this.y = y;
        this.hbWidth = w;
        this.hbHeight = h;
        health = Integer.MAX_VALUE;
    }


    //setters
    void setType(String type) {
        this.type = type;
    }
    void setX(int x) {
        this.x = x;
    }

    void setY(int y) {
        this.y = y;
    }

    void setHealth(int health) {
        this.health = health;
    }


    //getters
    String getType() {
        return type;
    }
    int getX() {
        return x;
    }
    int getY() {
        return y;
    }
    int getHealth() {
        return health;
    }

    boolean detectCollision(Entity other) {
        // logic
        return true;
    }

}