package model;

/**
 * holds information relating to the player
 * singleton design method
 */
class Player extends Entity {

	private static final int HITBOXWIDTH = 20;

	private static final int HITBOXHEIGHT = 20;

	Player(int x, int y) {
		super("PLAYER", x, y, HITBOXWIDTH, HITBOXHEIGHT);
	}

    //max that player's health can be
    private int maxHealth = 25;

    //player's inventory
    private Item[] inventory = new Item[10];

    //stats
    private int marksmanship;
    private int persuasion;
    private int hackery;
    private int stealth;

    void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }
    void setMarksmanship(int marksmanship) {
        this.marksmanship = marksmanship;
    }
    void setPersuasion(int persuasion) {
        this.persuasion = persuasion;
    }
    void setHackery(int hackery) {
        this.hackery = hackery;
    }
    void setStealth(int stealth) {
        this.stealth = stealth;
    }

    int getMaxHealth() {
        return maxHealth;
    }
    int getMarksmanship() {
        return marksmanship;
    }
    int getPersuasion() {
        return persuasion;
    }
    int getHackery() {
        return hackery;
    }
    int getStealth() {
        return stealth;
    }

    void editInventory(Item some, int where) {
        inventory[where] = some;
    }

    Item getFromInventory(int index) {
        return inventory[index];
    }

    int getInventoryLength() {
    	return inventory.length;
    }
}