package model;

/**
 * Item constructor
 * @author odelz
 */
class Item {
    private int quantity;
    private String type;

    private Item(String type, int quantity) {
        this.type = type;
        this.quantity = quantity;
    }
    //setters
    void setType(String type) {
        this.type = type;
    }
    void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    //getters
    String getType() {
        return type;
    }
    int getQuantity() {
        return quantity;
    }



}
