package model;

abstract class Interactable extends Entity {


    Interactable(String type, int x, int y, int w, int h) {
        super(type, x, y, w, h);
    }

    abstract String interact(String input);

}
